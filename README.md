# Log to HTML

Generate an HTML page from provided logs

## Dependencies
Jinja2  

## How to use it
Put logs under directory, [logs](logs), and the format should be
```
# item1.log
www.google.com,PASS
www.facebook.com,PASS
```


Generate HTML with 

```
python generator.py
```

## Result
The generated HTML, [result.html](www/result.html), will be located in directory [www](www).
```html
<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>item1</h2>
  <p>Click on the collapsible panel to open and close it.</p>
  <div class="panel-group">
    <div class="panel panel-danger">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" href="#item1">Test cases</a>
        </h4>
      </div>
      <div id="item1" class="panel-collapse collapse">
        <div class="panel-body">
          <div class="row">
            <div class="col-md-6">www.google.com</div><div class="col-md-6">FAIL</div>
          </div>
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-md-6">www.facebook.com</div><div class="col-md-6">PASS</div>
          </div>
        </div>
        </div>
    </div>
  </div>
</div>
<div class="container">
  <h2>item2</h2>
  <p>Click on the collapsible panel to open and close it.</p>
  <div class="panel-group">
    <div class="panel panel-primary">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" href="#item2">Test cases</a>
        </h4>
      </div>
      <div id="item2" class="panel-collapse collapse">
        <div class="panel-body">
          <div class="row">
            <div class="col-md-6">www.google.com</div><div class="col-md-6">PASS</div>
          </div>
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-md-6">www.facebook.com</div><div class="col-md-6">PASS</div>
          </div>
        </div>
        </div>
    </div>
  </div>
</div>
<div class="container">
  <h2>item3</h2>
  <p>Click on the collapsible panel to open and close it.</p>
  <div class="panel-group">
    <div class="panel panel-primary">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" href="#item3">Test cases</a>
        </h4>
      </div>
      <div id="item3" class="panel-collapse collapse">
        <div class="panel-body">
          <div class="row">
            <div class="col-md-6">www.google.com</div><div class="col-md-6">PASS</div>
          </div>
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-md-6">www.facebook.com</div><div class="col-md-6">PASS</div>
          </div>
        </div>
        </div>
    </div>
  </div>
</div>

</body>
</html>
```

