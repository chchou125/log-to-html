from jinja2 import Environment, FileSystemLoader
from os import listdir

import os.path


class Site:
    def __init__(self, _url, _result):
        self.url = _url
        self.result = _result


current_path = os.path.dirname(__file__)
template_path = '{}/templates/'.format(current_path)
log_path = '{}/logs/'.format(current_path)
html_path = '{}/www/'.format(current_path)
result_name = "result.html"

loader = FileSystemLoader(template_path)
env = Environment(loader=loader)
template = env.get_template("test.html")

log_file_list = [x for x in listdir(log_path) if x.endswith(".log")]
item_dict = {}

for l in log_file_list:
    f = open(log_path + l, "r")
    site_list = []
    for line in f.read().splitlines():
        url, result = line.split(',')
        site_list.append(Site(url, result))
    item_dict[os.path.splitext(l)[0]] = site_list
    f.close()

print template.render(items=item_dict)

f = open(html_path + result_name, "w")
f.write(template.render(items=item_dict))
f.close()
